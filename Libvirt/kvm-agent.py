import libvirt
import subprocess
import sys
import os.path
import uuid
import hashlib
import shutil
#import random
import string
from random import *
#from random import choice

function_name = sys._getframe().f_code.co_name

def runcommand (cmd):
    proc = subprocess.Popen(cmd,
                            stdout=subprocess.PIPE,
                            stderr=subprocess.PIPE,
                            shell=True,
                            universal_newlines=True)
    std_out, std_err = proc.communicate()
    return proc.returncode, std_out, std_err

def randomMAC():
    mac = [ 0x00, 0x16, 0x3e,
        randint(0x00, 0x7f),
        randint(0x00, 0xff),
        randint(0x00, 0xff) ]
    return ':'.join(map(lambda x: "%02x" % x, mac))

def match_md5(file1, file2):
    if not os.path.exists(file1):
        return "[ Error ] @ compare_2_files_md5 => file ("+ file1 +") does NOT exist"
    elif not os.path.isfile(file1):
        return "[ Error ] @ compare_2_files_md5 => file ("+ file1 +") is NOT a file"

    if not os.path.exists(file2):
        return "[ Error ] @ compare_2_files_md5 => file ("+ file2 +") does NOT exist"
    elif not os.path.isfile(file2):
        return "[ Error ] @ compare_2_files_md5 => file ("+ file2 +") is NOT a file"

    def md5(fname):
        hash_md5 = hashlib.md5()
        with open(fname, "rb") as f:
            for chunk in iter(lambda: f.read(4096), b""):
                hash_md5.update(chunk)
        return hash_md5.hexdigest()

    file1_md5 = md5(file1)
    file2_md5 = md5(file2)
    if file1_md5 == file2_md5:
        return True
    else:
        return False

#print(match_md5('/tmp/ES-PP.pem', '/tmp/bb.txt'))

def connection():
    conn = libvirt.open('qemu:///system')
    if conn == None:
        sys.stderr.write('Failed to open connection to qemu:///system')
        exit(1)
    else:
        return conn
conn = connection()

ovs_br = 'main0'
default_bridge = 'bridge0'
default_nat    = 'nat0'



def ovs_bridge_exists(bridge):
    cmd = runcommand('ovs-vsctl list-br')
    if cmd[0] == 0:
        arr = cmd[1].split("\n")
        arr1 = list(filter(None, arr))
        return bridge in arr1
    elif cmd[0] == 1:
        return "@ ovs_bridge_exists ( "+ bridge +" ) => failed" + "\n => " + str(cmd)

def namespace_exists(namespace):
    cmd = runcommand('ip netns list')
    if cmd[0] == 0:
        arr = cmd[1].split("\n")
        arr1 = list(filter(None, arr))
        return namespace in arr1
    elif cmd[0] == 1:
        return "@ namespace_exists ( "+ namespace +" ) => failed" + "\n => " + str(cmd)


#print(ovs_bridge_exists('bridge1'))

def ovs_create_bridge(bridge):

    if ovs_bridge_exists(bridge) == False:
        cmd = runcommand('ovs-vsctl add-br ' + bridge)
        if cmd[0] == 0:
            return 'bridge_create_success'
        else:
            runcommand('ovs-vsctl del-br ' + bridge)
            return 'bridge_create_failed'
    elif ovs_bridge_exists(bridge) == True:
        return "@ ovs_create_bridge ("+ bridge +") => already exists "

ovs_create_bridge(default_bridge)
ovs_create_bridge(default_nat)


def ovs_delete_bridge(bridge):
    if ovs_bridge_exists(bridge) == True:
        cmd = runcommand('ovs-vsctl del-br ' + bridge)
        if cmd[0] == 0:
            return 'bridge_delete_success'
        else:
            return 'bridge_delete_failed'


def domain_exists(domain_):
    arr = []
    domains = conn.listAllDomains(0)
    if len(domains) != 0:
        for domain in domains:
            arr.append (domain.name())
        return domain_ in arr
#print(domain_exists('love19'))

def domain_state(domain_name):
    if domain_exists(domain_name):
        dom = conn.lookupByName(domain_name)
        state, reason = dom.state()
        if state == libvirt.VIR_DOMAIN_NOSTATE:
            state = 'VIR_DOMAIN_NOSTATE'
        elif state == libvirt.VIR_DOMAIN_RUNNING:
            state = 'VIR_DOMAIN_RUNNING'
        elif state == libvirt.VIR_DOMAIN_BLOCKED:
            state = 'VIR_DOMAIN_BLOCKED'
        elif state == libvirt.VIR_DOMAIN_PAUSED:
            state = 'VIR_DOMAIN_PAUSED'
        elif state == libvirt.VIR_DOMAIN_SHUTDOWN:
            state = 'VIR_DOMAIN_SHUTDOWN'
        elif state == libvirt.VIR_DOMAIN_SHUTOFF:
            state = 'VIR_DOMAIN_SHUTOFF'
        elif state == libvirt.VIR_DOMAIN_CRASHED:
            state = 'VIR_DOMAIN_CRASHED'
        elif state == libvirt.VIR_DOMAIN_PMSUSPENDED:
            state = 'VIR_DOMAIN_PMSUSPENDED'
        else:
            state = "@ domain_state (" + domain_name + ") => unknown"
        return state
    elif not domain_exists(domain_name):
        return "@ domain_state (" + domain_name + ") => domain does NOT exist"

def domain_shutdown(domain_name):
    if domain_exists(domain_name):
        if domain_state(domain_name) == 'VIR_DOMAIN_SHUTDOWN' or domain_state(domain_name) == 'VIR_DOMAIN_SHUTOFF':
            out = 'Already_off'
        else:
            dom = conn.lookupByName(domain_name)
            if dom.shutdown() == 0:
                out = 'shutdown'
            else:
                out = 'failed_to_shutdown'
        return out
    else:
        return "domain_does_not_exist"

def domain_start(domain_name):
    if domain_exists(domain_name):
        if domain_state(domain_name) == 'VIR_DOMAIN_SHUTDOWN' or domain_state(domain_name) == 'VIR_DOMAIN_SHUTOFF':
            dom = conn.lookupByName(domain_name)
            if dom.create() == 0:
                out = 'start'
            else:
                out = 'failed_to_start'
        else:
            out = 'not_off'
        return out
    else:
        return "domain_does_not_exist"

def domain_reboot(domain_name):
    if domain_exists(domain_name):
        if not domain_state(domain_name) == 'VIR_DOMAIN_RUNNING':
            out = 'domain_not_running'
        else:
            dom = conn.lookupByName(domain_name)
            if dom.reboot() == 0:
                out = 'reboot'
            else:
                out = 'failed_to_reboot'
        return out
    else:
        return "domain_does_not_exist"

def domain_power_off(domain_name):
    if domain_exists(domain_name):
        if not domain_state(domain_name) == 'VIR_DOMAIN_RUNNING':
            out = 'domain_not_running'
        else:
            dom = conn.lookupByName(domain_name)
            if dom.destroy() == 0:
                out = 'destroy'
            else:
                out = 'failed_to_destroy'
        return out
    else:
        return "domain_does_not_exist"



def domain_add_interface(domain_name, bridge):
    if domain_exists(domain_name):
        if domain_state(domain_name) == 'VIR_DOMAIN_RUNNING':
            try:
                xml = """
                <interface type='bridge'>
                  <mac address='""" + randomMAC() + """'/>
                  <source bridge='""" + bridge + """'/>
                  <virtualport type='openvswitch'/>
                </interface>"""
                domain = conn.lookupByName(domain_name)
                domain.attachDevice(xml)
                return "[ INFO ]  Interface added; [domain: "+ domain_name +", bridge: "+ bridge +"]"
            except:
                return "@ domain_add_interface => ("+ domain_name +" > "+ bridge +") Failed to add interface"
        else:
            return 'domain_not_running'
    else:
        return 'domain_does_not_exist'


# print(domain_add_interface('node-01', 'main0'))


# print(conn.interfaceDefineXML(xml1, 0))


def get_vnc_port_number(domain_name):
    if domain_exists(domain_name):
        if domain_state(domain_name) == 'VIR_DOMAIN_RUNNING':
            from xml.etree import ElementTree as ET
            domain = conn.lookupByName(domain_name)

            # get the XML description of the VM
            vmXml = domain.XMLDesc(0)
            root = ET.fromstring(vmXml)

            # get the VNC port
            graphics = root.find('./devices/graphics')
            port = graphics.get('port')
            return port
        else:
            return 'domain_not_running'
    else:
        return 'domain_does_not_exist'

# print(get_vnc_port_number('node-011'))


#print(dir(conn))

def disk_create(name, size, location='/var/lib/libvirt/images', type='thin'):
    if not location.endswith("/"):
        location = location + "/"
    else:
        location = location
    if os.path.exists(location + name + ".qcow2"):
        return "[ Skip ]  @ disk_create => disk (" + location + name + ".qcow2) " + "already exists"
    else:
        if type == 'thin':
            create_1 = runcommand("qemu-img create -f vhdx " + location + name + ".qcow2 " + str(size) + "G")
            if create_1[0] != 0:
                location + name + ".qcow2"
                out = 'create_disk_failed'
            elif create_1[0] == 0:
                out = 'create_disk_success'
            create_2 = runcommand(
                "qemu-img convert " + location + name + ".qcow2" + " -O qcow2 " + location + name + ".qcow2")
            if create_2[0] != 0:
                os.remove(location + name + ".qcow2")
                out = 'create_disk_failed'
            elif create_2[0] == 0:
                out = 'create_disk_success'
            return out
        elif type == 'thick':
            sys.stdout.write('code not written yet')
            return None
#print(disk_create('love12', 20))


def domain_create_from_template(name,network, disk_source,new_disk_location='/var/lib/libvirt/images/', ram=1024, vcpus=1):
    if domain_exists(name):
        sys.stdout.write("[ Skip ]  @ domain_create_from_template => Domain ("+ name +") exists\n")
        return None
    if not ovs_bridge_exists(network):
        sys.stderr.write("[ Error ] @ domain_create_from_template => bridge ("+ network +") does NOT exists\n")
        return None

    if not os.path.exists(disk_source):
        sys.stderr.write("[ Error ] @ domain_create ->  disk_source (" + disk_source + "); does NOT exist\n")
        return
    elif os.path.isfile(disk_source) and not os.path.isfile(disk_source):
        sys.stderr.write("[ Error ] @ domain_create ->  disk_source (" + disk_source + "); is NOT a file\n")
        return

    if not os.path.exists(new_disk_location):
        return "[ Error ] @ domain_create_from_template => new_disk_location: (" + new_disk_location + ") does NOT exist"
    elif not os.path.isdir(new_disk_location):
        return "[ Error ] @ domain_create_from_template => new_disk_location: ("+ new_disk_location +") is NOT a directory"

    if not new_disk_location.endswith("/"):
        new_disk_location_ = new_disk_location + "/"
    else:
        new_disk_location_ = new_disk_location

    new_disk = new_disk_location_ + name + '.qcow2'

    def check_size_before_copy(src_file, dest_dir):
        total, used, free = shutil.disk_usage(dest_dir)
        dest_dir_free_space = int(free / 1024 / 1024)
        src_file_size = int(os.path.getsize(src_file) / 1024 / 1024)

        if src_file_size > dest_dir_free_space:
            return 'larger'
        else:
            return 'smaller'

    if check_size_before_copy(disk_source, new_disk_location_) == 'larger':
        return "[ Error ] @ " + sys._getframe().f_code.co_name + " => source disk: (" + disk_source + ") is larger than destination directory: (" + new_disk_location_ + ")"

    def copy_disk(src, dst):
        try:
            shutil.copy(src, dst)
        except:
            return "[ Error ] @ "+ sys._getframe().f_code.co_name +" => failed to copy disk ("+ src +") to "+ dst +" "

    copy_disk(disk_source, new_disk)

    if not match_md5(disk_source, new_disk):
        return "[ Error ] @ "+ sys._getframe().f_code.co_name +" => failed to copy disk ("+ src +") to "+ dst +" - match check Failed "

    disk_xml_template = """
            <disk device="disk" type="file">
                  <driver name='qemu' type='qcow2'/>
                  <source file='""" + new_disk + """'/>
                  <target dev='vda' bus='virtio'/>
                  <address type='pci' domain='0x0000' bus='0x00' slot='0x07' function='0x0'/>
            </disk>
            """
    xml = """
<domain type="kvm">
    <name>"""+ name +"""</name>
    <uuid>"""+ str(uuid.uuid4()) +"""</uuid>
    <memory unit="MB">"""+ str(ram) +"""</memory>
    <currentMemory unit="MB">"""+ str(ram) +"""</currentMemory>
    <vcpu placement="static">"""+ str(vcpus) +"""</vcpu>
    <os>
        <type>hvm</type>
        <boot dev="hd" />
    </os>
    <features>
        <acpi />
        <apic />
        <pae />
    </features>
    <cpu mode='host-model'>
      <model fallback='allow'/>
    </cpu>
    <clock offset="utc" />
    <on_poweroff>destroy</on_poweroff>
    <on_reboot>restart</on_reboot>
    <on_crash>restart</on_crash>
    <devices>"""+ disk_xml_template +"""<interface type='bridge'>
            <mac address='"""+ randomMAC() +"""'/>
            <source bridge='"""+ ovs_br +"""'/>
            <virtualport type='openvswitch'/>
            <address type='pci' domain='0x0000' bus='0x00' slot='0x03' function='0x0'/>
        </interface>
        <serial type='pty'>
          <target port='0'/>
        </serial>
        <console type='pty'>
          <target type='serial' port='0'/>
        </console>
        <channel type='unix'>
          <source mode='bind'/>
          <target type='virtio' name='org.qemu.guest_agent.0'/>
          <address type='virtio-serial' controller='0' bus='0' port='1'/>
        </channel>
        <channel type='spicevmc'>
          <target type='virtio' name='com.redhat.spice.0'/>
          <address type='virtio-serial' controller='0' bus='0' port='2'/>
        </channel>
        <input type='tablet' bus='usb'/>
        <input type='mouse' bus='ps2'/>
        <input type='keyboard' bus='ps2'/>
        <graphics type='spice' autoport='yes'>
          <image compression='off'/>
        </graphics>
        <sound model='ich6'>
          <address type='pci' domain='0x0000' bus='0x00' slot='0x04' function='0x0'/>
        </sound>
    </devices>
</domain>
    """
    #print(xml)
    conn.defineXML(xml)
    if domain_exists(name):
        sys.stdout.write("domain_create_success\n")
    else:
        domain = conn.lookupByName(name)
        domain.undefine()
        sys.stderr.write("domain_create_failed\n")
        return None

#print(dir(conn))

# print(ovs_create_bridge('test'))


def ovs_create_bridge_dhcp(bridge_name):
    ns_name  = bridge_name + '-ns'
    random_number = randint(50, 300)
    random_letter = choice(string.ascii_lowercase)
    ovs_port = ns_name + str(random_number) + random_letter
    if not ovs_bridge_exists(bridge_name):

        if ovs_create_bridge(bridge_name) == 'bridge_create_success':
            # Create Network Namespace
            if namespace_exists(ns_name):
                ovs_delete_bridge(bridge_name)
                return "[ Error ]  @ ovs_create_bridge_dhcp => Failed to create Namespace => namespace exits ("+ ns_name +")"
            create_ns = runcommand("ip netns add "+ bridge_name + "-ns")
            if create_ns[0] != 0:
                # Rollback
                runcommand("ip netns del " + ns_name)
                ovs_delete_bridge(bridge_name)
                return "[ Error ]  @ ovs_create_bridge_dhcp => Failed to create Namespace\n" + str(create_ns)
            # Create ovs port
            create_ovs_port = runcommand("ovs-vsctl add-port "+ bridge_name +" "+ ovs_port +" -- set Interface "+ ovs_port +" type=internal")
            if create_ovs_port[0] == 0:
                attach_port_to_ns = runcommand("ip link set "+ ovs_port +" netns " + ns_name)
                if attach_port_to_ns[0] == 0:
                    set_port_up = runcommand("ip netns exec "+ ns_name +" ip link set dev "+ ovs_port +" up")
                    if set_port_up[0] == 0:
                        runcommand("ip netns exec "+ ns_name +" ip link set dev lo up")
                        return 'bridge_created'
                    else:
                        runcommand("ip netns del " + ns_name)
                        ovs_delete_bridge(bridge_name)
                        return "[ Error ]  @ ovs_create_bridge_dhcp => Failed to set port to up => (" + ovs_port + ")" + "\n" + str(attach_port_to_ns) + "\n" + str(set_port_up)
                else:
                    runcommand("ip netns del " + ns_name)
                    ovs_delete_bridge(bridge_name)
                    return "[ Error ]  @ ovs_create_bridge_dhcp => Failed to attach port ("+ ovs_port +") to namespace ("+ ns_name +")" + "\n" + str(attach_port_to_ns)
            else:
                # Rollback
                runcommand("ip netns del " + ns_name)
                ovs_delete_bridge(bridge_name)
                return "[ Error ]  @ ovs_create_bridge_dhcp => (" + bridge_name + ") Failed to create port => ("+ ovs_port +")" + "\n" + str(create_ovs_port)
        else:
            # Rollback
            runcommand("ip netns del " + ns_name)
            ovs_delete_bridge(bridge_name)
            return "[ Error ]  @ ovs_create_bridge_dhcp => ("+ bridge_name +") Failed to create bridge"
    elif ovs_bridge_exists(bridge_name):
        return 'bridge_exists'


#print(ovs_create_bridge_dhcp('love14'))

#print(namespace_exists('test1'))

print(disk_create(name='test1', size=50))

print(domain_create_from_template(name='love21',network='main1', disk_source='/tmp/centos.qcow2', new_disk_location='/var/lib/libvirt/images/',ram=4096, vcpus=2))


## Functions Todo

def domain_info():
    pass


#check_size_before_copy('/root/kubectl.zip', '/tmp')

#print(domain_shutdown('love21'))
#print(domain_start('love21'))
#print(domain_reboot('love21'))
print(domain_power_off('love21'))



