from prometheus_client.parser import text_string_to_metric_families
import requests

def metrics_list(node_url):
    try:
        url = requests.get(node_url, timeout=3)
    except Exception as e:
        return "can NOT connect to: "+ node_url + "\n" + str(e)

    metric_list = []
    for family in text_string_to_metric_families(url.text):
        for sample in family.samples:
            metric_list.append({"name": sample[0], "labels": sample[1], "value": sample[2]})
    return metric_list

def metrics_search(node_url, metric_name):
    try:
        url = requests.get(node_url, timeout=3)
    except Exception as e:
        return "can NOT connect to: "+ node_url + "\n" + str(e)

    metric_list = []
    for family in text_string_to_metric_families(url.text):
        for sample in family.samples:
            metric_list.append({"name": sample[0], "labels": sample[1], "value": sample[2]})
    metric_list_ = metric_list

    try:
        for p in metric_list_:
            if p['name'] == metric_name:
                return p
    except Exception as e:
        return "can't search metric: " + metric_name + "\n" + e


#print(metrics_list('http://localhost:9100/metrics'))
#print(metrics_search('http://localhost:9100/metrics', 'node_vmstat_pgmajfault'))


